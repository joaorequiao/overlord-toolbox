﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

namespace com.overlordgamestudio.OverlordToolbox.Input
{
    public enum InputDirection
    {
        Up,
        Down,
        Left,
        Right
    }

    public static class OverlordInputManager
    {
        public static MainInputActions controls;

        public static void Init()
        {
            controls = new MainInputActions();
            controls.Enable();

            anyKey = new InputAction(binding: "/*/<button>");
            anyKey.Enable();

        }

        static InputAction anyKey;

        public static float inputThreshold_High = 0.9f;
        public static float inputThreshold_Mid = 0.5f;
        public static float inputThreshold_Low = 0.3f;

        #region Helper Functions

        public static void EnableGameModeMaps()
        {
            if (controls == null)
            {
                Init();
            }
            
            controls.InGame.Enable();
            controls.Menu.Disable();
        }

        public static void EnableMenusModeMaps()
        {
            if (controls == null)
            {
                Init();
            }

            controls.InGame.Disable();
            controls.Menu.Enable();
        }

        public static void DisableAllModeMaps()
        {
            if (controls == null)
            {
                Init();
            }
            
            controls.InGame.Disable();
            controls.Menu.Disable();
        }

        public static bool InGameMapsEnabled() 
        {
            return controls.InGame.enabled;
        }

        public static bool MenuMapsEnabled()
        {
            return controls.Menu.enabled;
        }

        #endregion

        #region Vibration Functions

        public static void SetStrongVibration(float duration)
        {
            //inputPlayer.SetVibration(0, 1f, duration);
            //inputPlayer.SetVibration(1, 1f, duration);
        }

        public static void SetMediumVibration(float duration)
        {

            //inputPlayer.SetVibration(0, 0.6f, duration);
            //inputPlayer.SetVibration(1, 0.6f, duration);
        }

        public static void SetWeakVibration(float duration)
        {
            // //inputPlayer.SetVibration(0, 1f, duration);
            //inputPlayer.SetVibration(1, 0.6f, duration);
        }

        #endregion


        #region In-Game Controll Map

        public static float Horizontal()
        {
            float value = controls.InGame.Movement.ReadValue<Vector2>().x;
            return (value >= inputThreshold_Low || value <= -inputThreshold_Low) ? value : 0;
        }

        public static float Horizontal_DPad()
        {
            if (controls.InGame.DPad.ReadValue<Vector2>().x > 0)
                return 1;
            else if (controls.InGame.DPad.ReadValue<Vector2>().x < 0)
                return -1;
            else
                return 0;
        }

        public static float Vertical()
        {
            float value = controls.InGame.Movement.ReadValue<Vector2>().y;
            return (value >= inputThreshold_Low || value <= -inputThreshold_Low) ? value : 0;
        }

        public static float Vertical_DPad()
        {
            if (controls.InGame.DPad.ReadValue<Vector2>().y > 0)
                return 1;
            else if (controls.InGame.DPad.ReadValue<Vector2>().y < 0)
                return -1;
            else
                return 0;
        }

        public static bool Glide()
        {
            return controls.InGame.Glide.ReadValue<float>() > 0;
        }

        public static bool Dash()
        {
            return controls.InGame.Dash.triggered;
        }

        public static bool Jump()
        {
            return controls.InGame.Jump.triggered;
        }

        public static bool Attack()
        {
            return controls.InGame.Attack.triggered;
        }

        public static bool HeavyAttack()
        {
            return controls.InGame.HeavyAttack.triggered;
        }

        public static bool RangedAttack()
        {
            return controls.InGame.RangedAttack.triggered;
        }

        public static bool UseBrew()
        {
            return controls.InGame.UseBrew.triggered;
        }

        public static bool Brewstone()
        {
            return controls.InGame.Brewstone.triggered;
        }

        public static bool Action()
        {
            return Vertical() > inputThreshold_Mid;
        }

        public static bool NextItem()
        {
            return controls.InGame.NextItem.triggered;
        }

        public static bool PreviousItem()
        {
            return controls.InGame.PreviousItem.triggered;
        }

        public static bool OpenMenu()
        {
            return controls.InGame.OpenMenu.triggered;
        }

        #endregion

        #region Menus Control Map

        public static float HorizontalMenu()
        {
            if (controls.Menu.Movement_Menu.ReadValue<Vector2>().x > 0)
                return 1;
            else if (controls.Menu.Movement_Menu.ReadValue<Vector2>().x < 0)
                return -1;
            else
                return 0;
        }

        public static float VerticalMenu()
        {
            if (controls.Menu.Movement_Menu.ReadValue<Vector2>().y > 0)
                return 1;
            else if (controls.Menu.Movement_Menu.ReadValue<Vector2>().y < 0)
                return -1;
            else
                return 0;
        }

        public static bool Confirm()
        {
            return controls.Menu.Confirm.triggered;
        }

        public static bool AnyButton()
        {
            return anyKey.triggered;
        }

        public static bool Cancel()
        {
            return controls.Menu.Cancel.triggered;
        }

        public static bool NextTab()
        {
            return controls.Menu.NextPage.ReadValue<float>() > inputThreshold_Mid; //.triggered;
        }

        public static bool PreviousTab()
        {
            return controls.Menu.PreviousPage.ReadValue<float>() > inputThreshold_Mid;
        }

        public static bool NextScreen()
        {
            return controls.Menu.NextPage.triggered;
        }

        public static bool PreviousScreen()
        {
            return controls.Menu.PreviousPage.triggered;
        }

        public static bool Start()
        {
            return controls.Menu.Start.triggered;
        }

        public static bool Select()
        {
            return controls.Menu.Select.triggered;
        }

        public static int DPadHorizontal_Menu()
        {
            if (controls.Menu.DPad_Menu.ReadValue<Vector2>().x > 0)
                return 1;
            else if (controls.Menu.DPad_Menu.ReadValue<Vector2>().x < 0)
                return -1;
            else
                return 0;
        }

        public static int DPadVertical_Menu()
        {
            if (controls.Menu.DPad_Menu.ReadValue<Vector2>().x > 0)
                return 1;
            else if (controls.Menu.DPad_Menu.ReadValue<Vector2>().x < 0)
                return -1;
            else
                return 0;
        }

        public static bool LeaveMenu()
        {
            return controls.Menu.LeaveMenu.triggered;
        }

        #endregion



        public static bool GetButtonHold(InputButtons btn)
        {
            if (btn == InputButtons.Confirm)
                return controls.Menu.Confirm.ReadValue<float>() > 0;

            if (btn == InputButtons.Cancel)
                return controls.Menu.Cancel.ReadValue<float>() > 0;

            return false;
        }
    }

    public enum InputButtons
    {
        None,
        Action,
        Directional_Up,
        Directional_Down,
        Directional_Left,
        Directional_Right,
        Jump,
        Dash,
        Glide,
        Cancel,
        Confirm,
        Attack,
        HeavyAttack,
        NextPage_UI,
        PreviousPage_UI,
        Heal,
        Inventory,
        Pause,
        RangedAttack,
        Brewstone
    }
}